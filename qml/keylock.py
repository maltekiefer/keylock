import os
import gnupg

def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

def generateKey(email, password):

    if not os.path.exists('/home/phablet/.local/share/keylock.maltekiefer/keys'):
            os.makedirs('/home/phablet/.local/share/keylock.maltekiefer/keys')

    gpg = gnupg.GPG(gnupghome='/home/phablet/.local/share/keylock.maltekiefer/keys') 
    
    input_data = gpg.gen_key_input(key_type="RSA", key_length=4096, name_email=email,passphrase=password)
    key = gpg.gen_key(input_data)

    return key
