import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'keylock.maltekiefer'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Keylock')
        }

        Column {
            id: configuration
            anchors.fill: parent
            
            Button {
                text: "Ok"
                onClicked: generateKey()
            }
            Label {
                id: lb
                text: "test"
            }
        }
    }

        function generateKey() {
            python.call('keylock.generateKey',['malte.kiefer@test.de','Hello World!'], function(returnValue) {
                    lb.text = returnValue
                })

    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('keylock', function() {
                console.log("-------------- python loaded");
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
