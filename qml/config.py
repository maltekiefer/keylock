import os

def get_path():
    DIR = os.path.abspath(os.path.curdir)
    APP_ID = "keylock.maltekiefer"

    if os.environ.get('XDG_DATA_HOME'):
        DIR = os.path.join(os.environ['XDG_DATA_HOME'], APP_ID)

    return DIR


def create_path(filename):
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)